//
//  MTViewController.swift
//  iOS_SWIFT_MVVM
//
//  Created by mint on 16/01/2018.
//  Copyright © 2018 maktub. All rights reserved.
//

import UIKit

class MTViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textField = UITextField()
        textField.backgroundColor = UIColor.yellow
        view.addSubview(textField)
        
        textField.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(50)
            make.center.equalTo(view)
        }
        

        let searchResults = textField.rx.text.orEmpty

        
        
        
        
        
        let label = UILabel();
        label.backgroundColor = UIColor.red
        view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.height.equalTo(50)
            make.top.equalTo(textField.snp.bottom)
            make.centerX.equalTo(textField)
        }
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
