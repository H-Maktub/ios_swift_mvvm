//
//  MTNavController.swift
//  iOS_SWIFT_MVVM
//
//  Created by Maktub on 2018/1/1.
//  Copyright © 2018年 Maktub. All rights reserved.
//

import UIKit

class MTNavController: UINavigationController, UINavigationControllerDelegate {
    
    var popDeletge:UIGestureRecognizerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        popDeletge = interactivePopGestureRecognizer?.delegate
        delegate = self;
        navigationBar.titleTextAttributes = [.foregroundColor:UIColor.lightGray,.font:UIFont.systemFont(ofSize: 17.0)];
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if childViewControllers.count != 0 {
            viewController.hidesBottomBarWhenPushed = true;
            
            let popPreVc = UIBarButtonItem.mt_barButtonItemWithImage("", highImageName: "", target: self, action: #selector(popToPreVc))
            viewController.navigationItem.leftBarButtonItem = popPreVc;
        }
        super.pushViewController(viewController, animated: true)
    }
    
    @objc func popToPreVc() {
        self.popViewController(animated: true)
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if viewController == viewControllers[0] {
            interactivePopGestureRecognizer?.delegate = popDeletge
        }else{
            interactivePopGestureRecognizer?.delegate = nil
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
